FROM golang:alpine
LABEL maintainer="GizatulinsO"
WORKDIR /app

COPY go.mod go.sum ./
RUN go mod download

COPY . .
##
# RUN ./jet-builder --o src/generated-sql
RUN go build -o main ./src

EXPOSE 8080

CMD ["./main"]
# ENTRYPOINT ["/bin/ash"]