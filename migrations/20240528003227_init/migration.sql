-- CreateTable
CREATE TABLE "events" (
    "id" SERIAL NOT NULL,
    "creatorClerkId" TEXT NOT NULL,
    "title" TEXT NOT NULL,
    "description" TEXT,
    "language" TEXT NOT NULL DEFAULT 'en',
    "color" TEXT,
    "scheduledAt" TIMESTAMP(3),
    "scheduledAtTime" TEXT,
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updatedAt" TIMESTAMP(3),
    "maxPlayers" INTEGER,
    "maxGms" INTEGER,
    "isPrivate" BOOLEAN NOT NULL DEFAULT false,

    CONSTRAINT "events_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "event_users" (
    "id" SERIAL NOT NULL,
    "eventId" INTEGER NOT NULL,
    "clerkId" TEXT NOT NULL,
    "userName" TEXT NOT NULL,
    "firstName" TEXT,
    "lastName" TEXT,
    "status" TEXT,
    "email" TEXT,
    "imageUrl" TEXT,
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updatedAt" TIMESTAMP(3),

    CONSTRAINT "event_users_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "event_user_join_requests" (
    "id" SERIAL NOT NULL,
    "eventId" INTEGER NOT NULL,
    "event_user_id" INTEGER NOT NULL,
    "text" TEXT,
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updatedAt" TIMESTAMP(3),

    CONSTRAINT "event_user_join_requests_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "event_invite_links" (
    "id" SERIAL NOT NULL,
    "eventId" INTEGER NOT NULL,
    "token" TEXT NOT NULL,
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updatedAt" TIMESTAMP(3),

    CONSTRAINT "event_invite_links_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "comments" (
    "id" SERIAL NOT NULL,
    "clerkId" TEXT NOT NULL,
    "userName" TEXT NOT NULL,
    "eventId" INTEGER NOT NULL,
    "parentId" INTEGER,
    "content" TEXT NOT NULL,
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updatedAt" TIMESTAMP(3),

    CONSTRAINT "comments_pkey" PRIMARY KEY ("id")
);

-- CreateIndex
CREATE UNIQUE INDEX "event_users_eventId_clerkId_key" ON "event_users"("eventId", "clerkId");

-- CreateIndex
CREATE UNIQUE INDEX "event_user_join_requests_event_user_id_key" ON "event_user_join_requests"("event_user_id");

-- CreateIndex
CREATE UNIQUE INDEX "event_invite_links_token_key" ON "event_invite_links"("token");

-- AddForeignKey
ALTER TABLE "event_users" ADD CONSTRAINT "event_users_eventId_fkey" FOREIGN KEY ("eventId") REFERENCES "events"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "event_user_join_requests" ADD CONSTRAINT "event_user_join_requests_eventId_fkey" FOREIGN KEY ("eventId") REFERENCES "events"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "event_user_join_requests" ADD CONSTRAINT "event_user_join_requests_event_user_id_fkey" FOREIGN KEY ("event_user_id") REFERENCES "event_users"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "event_invite_links" ADD CONSTRAINT "event_invite_links_eventId_fkey" FOREIGN KEY ("eventId") REFERENCES "events"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "comments" ADD CONSTRAINT "comments_eventId_fkey" FOREIGN KEY ("eventId") REFERENCES "events"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "comments" ADD CONSTRAINT "comments_parentId_fkey" FOREIGN KEY ("parentId") REFERENCES "comments"("id") ON DELETE CASCADE ON UPDATE CASCADE;
