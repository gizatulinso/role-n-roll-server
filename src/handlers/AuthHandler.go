package handlers


import (
	clerkhttp "github.com/clerk/clerk-sdk-go/v2/http"
	"os"
	"github.com/clerk/clerk-sdk-go/v2"
	"github.com/clerk/clerk-sdk-go/v2/user"
	"net/http"
	"context"
)

const (
	API_KEY = "CLERK_SECRET_KEY"
)

func WithClerkSession(next http.Handler) http.Handler {
	apiKey := os.Getenv("CLERK_SECRET_KEY")
	clerk.SetKey(apiKey)
	handler := clerkhttp.WithHeaderAuthorization()
	return handler(next)
}

func GetUserId(r *http.Request) string {
	claims, ok := clerk.SessionClaimsFromContext(r.Context())
	if !ok {
		return ""
	}
	return claims.Subject
}

func IsAuthorized(r *http.Request, expectedClerkID string) bool {
	sessionClerkID := GetUserId(r)
	if sessionClerkID == "" || sessionClerkID != expectedClerkID {
		return false
	}
	return true
}

func isAuthenticaded(r *http.Request) bool {
	_, ok := clerk.SessionClaimsFromContext(r.Context())
	return ok
}

func GetClerkUser(clerkID string, ctx context.Context) (*clerk.User, error) {
	return user.Get(ctx, clerkID)
}