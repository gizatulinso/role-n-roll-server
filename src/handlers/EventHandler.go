package handlers

import (
	"context"
	"crypto/rand"
	"database/sql"
	"encoding/hex"
	"errors"
	"log"
	"net/http"
	"strconv"
	// "github.com/clerk/clerk-sdk-go/v2"
	"github.com/go-chi/chi/v5"
	"github.com/go-chi/render"
	"gitlab.com/gizatulinso/role-n-roll-server/src/dto"
	"gitlab.com/gizatulinso/role-n-roll-server/src/models"
	"gitlab.com/gizatulinso/role-n-roll-server/src/repositories"
)

type EventHandler interface {
	GetEvent(w http.ResponseWriter, r *http.Request)
	GetAllEvents(w http.ResponseWriter, r *http.Request)
	CreateEvent(w http.ResponseWriter, r *http.Request)
	DeleteEvent(w http.ResponseWriter, r *http.Request)
	UpdateEvent(w http.ResponseWriter, r *http.Request)
	AddUserToEvent(w http.ResponseWriter, r *http.Request)
	RemoveUserFromEvent(w http.ResponseWriter, r *http.Request)
	UpdateUserStatus(w http.ResponseWriter, r *http.Request)
	GetComments(w http.ResponseWriter, r *http.Request)
	AddCommentToEvent(w http.ResponseWriter, r *http.Request)
	UpdateComment(w http.ResponseWriter, r *http.Request)
	DeleteComment(w http.ResponseWriter, r *http.Request)
	CreateInviteLink(w http.ResponseWriter, r *http.Request)
	AddUserByInviteLink(w http.ResponseWriter, r *http.Request)
	// WithEventCtx Key: event, Value: Event
	WithEventCtx(next http.Handler) http.Handler
	// WithCommentCtx Key: comment, Value: Comment
	WithCommentCtx(next http.Handler) http.Handler
}

type EventHandlerImpl struct {
	DB   *sql.DB
	Log  *log.Logger
	Repo repositories.EventRepository
}

func NewEventHandler(db *sql.DB, log *log.Logger) *EventHandlerImpl {
	return &EventHandlerImpl{
		DB:   db,
		Log:  log,
		Repo: repositories.NewEventRepository(db),
	}
}

func (h *EventHandlerImpl) WithEventCtx(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		var Event *models.Event

		if EventID := chi.URLParam(r, "ID"); EventID != "" {
			ID, err := strconv.Atoi(EventID)
			if err != nil {
				render.Render(w, r, dto.ErrInvalidRequest(err))
				return
			}
			Event, err = h.Repo.GetByID(ID)
			if err != nil {
				render.Render(w, r, dto.ErrNotFound)
				return 
			}

			ctx := context.WithValue(r.Context(), "event", Event)
			next.ServeHTTP(w, r.WithContext(ctx))
		} else {
			err := errors.New("Event ID is required")
			render.Render(w, r, dto.ErrInvalidRequest(err))
		}
	})
}

func (h *EventHandlerImpl) WithCommentCtx(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		var Comment *models.Comment

		if CommentID := chi.URLParam(r, "CommentID"); CommentID != "" {
			ID, err := strconv.Atoi(CommentID)
			if err != nil {
				render.Render(w, r, dto.ErrInvalidRequest(err))
				return
			}
			Comment, err = h.Repo.GetComment(int32(ID))
			if err != nil {
				render.Render(w, r, dto.ErrNotFound)
				return 
			}

			ctx := context.WithValue(r.Context(), "comment", Comment)
			next.ServeHTTP(w, r.WithContext(ctx))
		} else {
			err := errors.New("Comment ID is required")
			render.Render(w, r, dto.ErrInvalidRequest(err))
		}
	})
}

func (h *EventHandlerImpl) GetEvent(w http.ResponseWriter, r *http.Request) {
	Event := r.Context().Value("event").(*models.Event)
	err := render.Render(w, r, dto.NewEventReponse(Event))
	if err != nil {
		render.Render(w, r, dto.ErrInvalidRequest(err))
	}
}

func (h *EventHandlerImpl) GetAllEvents(w http.ResponseWriter, r *http.Request) {
	// Extract page number and items per page from query parameters
	page, _ := strconv.Atoi(r.URL.Query().Get("page"))
	itemsPerPage, _ := strconv.Atoi(r.URL.Query().Get("itemsPerPage"))

	// Default values
	if page < 1 {
		page = 1
	}
	if itemsPerPage < 1 {
		itemsPerPage = 10
	}

	// Pass page number and items per page to GetAll function
	events, err := h.Repo.GetAll(page, itemsPerPage)
	if err != nil {
		render.Render(w, r, dto.ErrInvalidRequest(err))
		return
	}
	render.RenderList(w, r, dto.NewEventListResponse(events))
}

func (h *EventHandlerImpl) CreateEvent(w http.ResponseWriter, r *http.Request) {
	data := &dto.POSTEventRequest{}

	if !isAuthenticaded(r) {
		render.Render(w, r, dto.ErrNotAuthorized)
		return
	}

	if err := render.Bind(r, data); err != nil {
		render.Render(w, r, dto.ErrInvalidRequest(err))
		return
	}

	Event := data.Event
	Event.CreatorClerkId = GetUserId(r)
	if id, err := h.Repo.Save(Event); err != nil {
		h.Log.Println((err.Error()))
		render.Render(w, r, dto.ErrInternalServer)
	} else {
		m := map[string]int32{"id": id}
		render.Status(r, http.StatusCreated)
		render.JSON(w, r, m)
	}
}

func (h *EventHandlerImpl) DeleteEvent(w http.ResponseWriter, r *http.Request) {
	Event := r.Context().Value("event").(*models.Event)
	
	if isAuthorized := IsAuthorized(r, Event.CreatorClerkId); !isAuthorized {
		render.Render(w, r, dto.ErrNotAuthorized)
		return
	}

	if err := h.Repo.Delete(Event.ID); err != nil {
		h.Log.Println((err.Error()))
		render.Render(w, r, dto.ErrInternalServer)
		return
	}
	render.Status(r, http.StatusNoContent)
}

func (h *EventHandlerImpl) UpdateEvent(w http.ResponseWriter, r *http.Request) {
	Event := r.Context().Value("event").(*models.Event)
	
	if isAuthorized := IsAuthorized(r, Event.CreatorClerkId); !isAuthorized {
		render.Render(w, r, dto.ErrNotAuthorized)
		return
	}

	data := &dto.PUTEventRequest{}
	if err := render.Bind(r, data); err != nil {
		render.Render(w, r, dto.ErrInvalidRequest(err))
		return
	}

	if data.Description != "" {
		Event.Description = &data.Description
	}
	if data.Title != "" {
		Event.Title = data.Title
	}
	if !data.ScheduledAt.IsZero() {
		Event.ScheduledAt = &data.ScheduledAt
	}
	if data.ScheduledAtTime != "" {
		if data.ScheduledAtTime == "0" {
			Event.ScheduledAtTime = nil
		} else {
			Event.ScheduledAtTime = &data.ScheduledAtTime
		}
	}
	if data.Language != "" {
		Event.Language = data.Language
	}
	if data.IsPrivate {
		Event.IsPrivate = data.IsPrivate
	}	
	if err := h.Repo.Update(Event); err != nil {
		h.Log.Println((err.Error()))
		render.Render(w, r, dto.ErrInternalServer)
		return
	}

	render.Status(r, http.StatusNoContent)
}

func (h *EventHandlerImpl) AddUserToEvent(w http.ResponseWriter, r *http.Request) {
	Event := r.Context().Value("event").(*models.Event)

	if isAuthorized := IsAuthorized(r, Event.CreatorClerkId); !isAuthorized {
		render.Render(w, r, dto.ErrNotAuthorized)
		return
	}

	data := &dto.POSTEventUserRequest{}
	if err := render.Bind(r, data); err != nil {
		render.Render(w, r, dto.ErrInvalidRequest(err))
		return
	}
	
	user := data.User
	if userID, err := h.Repo.AddUser(Event.ID, user); err != nil {
		h.Log.Println((err.Error()))
		render.Render(w, r, dto.ErrInternalServer)
		return
	} else {
		m := map[string]int32{"id": userID}
		render.Status(r, http.StatusCreated)
		render.JSON(w, r, m)
	}
}

func (h *EventHandlerImpl) RemoveUserFromEvent(w http.ResponseWriter, r *http.Request) {
	Event := r.Context().Value("event").(*models.Event)
	
	if isAuthorized := IsAuthorized(r, Event.CreatorClerkId); !isAuthorized {
		render.Render(w, r, dto.ErrNotAuthorized)
		return
	}

	userID, _ := strconv.Atoi(chi.URLParam(r, "UserID"))
	if err := h.Repo.DeleteUser(int32(userID)); err != nil {
		h.Log.Println((err.Error()))
		render.Render(w, r, dto.ErrInternalServer)
		return
	}
	render.Status(r, http.StatusNoContent)
}


func (h *EventHandlerImpl) UpdateUserStatus(w http.ResponseWriter, r *http.Request) {
	Event := r.Context().Value("event").(*models.Event)

	if isAuthorized := IsAuthorized(r, Event.CreatorClerkId); !isAuthorized {
		render.Render(w, r, dto.ErrNotAuthorized)
		return
	}

	userID, _ := strconv.Atoi(chi.URLParam(r, "UserID"))
	data := &dto.PUTUserStatusRequest{}
	if err := render.Bind(r, data); err != nil {
		render.Render(w, r, dto.ErrInvalidRequest(err))
		return
	}
	if err := h.Repo.UpdateUserStatus(int32(userID), data.Status); err != nil {
		h.Log.Println((err.Error()))
		render.Render(w, r, dto.ErrInternalServer)
		return
	}
	render.Status(r, http.StatusNoContent)
}

func (h *EventHandlerImpl) GetComments(w http.ResponseWriter, r *http.Request) {
	Event := r.Context().Value("event").(*models.Event)
	comments, err := h.Repo.GetComments(Event.ID)
	if err != nil {
		render.Render(w, r, dto.ErrInternalServer)
		return
	}
	render.RenderList(w, r, dto.NewCommentListResponse(comments))
}

func (h *EventHandlerImpl) AddCommentToEvent(w http.ResponseWriter, r *http.Request) {
	Event := r.Context().Value("event").(*models.Event)

	if !isAuthenticaded(r) {
		render.Render(w, r, dto.ErrNotAuthorized)
		return
	}

	data := &dto.POSTCommentRequest{}
	if err := render.Bind(r, data); err != nil {
		render.Render(w, r, dto.ErrInvalidRequest(err))
		return
	}

	comment := &models.Comment{}
	comment.ClerkId = GetUserId(r)
	comment.UserName = data.UserName
	comment.EventId = data.EventId
	comment.ParentId = data.ParentId
	comment.Content = data.Content

	if id, err := h.Repo.AddComment(Event.ID, comment); err != nil {
		h.Log.Println((err.Error()))
		render.Render(w, r, dto.ErrInternalServer)
		return
	} else {
		m := map[string]int32{"id": id}
		render.Status(r, http.StatusCreated)
		render.JSON(w, r, m)
	}
}

func (h *EventHandlerImpl) DeleteComment(w http.ResponseWriter, r *http.Request) {
	comment := r.Context().Value("comment").(*models.Comment)
	if isAuthorized := IsAuthorized(r, comment.ClerkId); !isAuthorized {
		render.Render(w, r, dto.ErrNotAuthorized)
		return
	}

	commentID, _ := strconv.Atoi(chi.URLParam(r, "CommentID"))
	if err := h.Repo.DeleteComment(int32(commentID)); err != nil {
		h.Log.Println((err.Error()))
		render.Render(w, r, dto.ErrInternalServer)
		return
	}
	render.Status(r, http.StatusNoContent)
}

func (h *EventHandlerImpl) UpdateComment(w http.ResponseWriter, r *http.Request) {
	Comment := r.Context().Value("comment").(*models.Comment)

	if isAuthorized := IsAuthorized(r, Comment.ClerkId); !isAuthorized {
		render.Render(w, r, dto.ErrNotAuthorized)
		return
	}

	data := &dto.PUTCommentRequest{}
	if err := render.Bind(r, data); err != nil {
		render.Render(w, r, dto.ErrInvalidRequest(err))
		return
	}

	Comment.Content = data.Content
	if err := h.Repo.UpdateComment(Comment); err != nil {
		h.Log.Println((err.Error()))
		render.Render(w, r, dto.ErrInternalServer)
		return
	}

	render.Status(r, http.StatusNoContent)
}

func (h *EventHandlerImpl) CreateInviteLink(w http.ResponseWriter, r *http.Request) {
	Event := r.Context().Value("event").(*models.Event)

	if isAuthorized := IsAuthorized(r, Event.CreatorClerkId); !isAuthorized {
		render.Render(w, r, dto.ErrNotAuthorized)
		return
	}

	token, err := GenerateRandomString()
	if err != nil {
		h.Log.Println((err.Error()))
		render.Render(w, r, dto.ErrInternalServer)
		return
	}

	if err := h.Repo.AddInviteLink(Event.ID, token); err != nil {
		h.Log.Println((err.Error()))
		render.Render(w, r, dto.ErrInternalServer)
		return
	}

	m := map[string]string{"token": token}
	render.Status(r, http.StatusCreated)
	render.JSON(w, r, m)
}

func (h *EventHandlerImpl) AddUserByInviteLink (w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	Event := r.Context().Value("event").(*models.Event)
	token := chi.URLParam(r, "Token")
	clerkUser, err := GetClerkUser(GetUserId(r), ctx)
	if err != nil {
		render.Render(w, r, dto.ErrNotAuthorized)
		return
	}

	var inviteExist bool 
	for _, link := range Event.EventInviteLinks {
		if link.Token == token {
			inviteExist = true
			break
		}
	}
	if !inviteExist {
		render.Render(w, r, dto.ErrNotFound)
		return
	}


	var userExists bool
	for _, user := range Event.EventUsers {
		if user.ClerkId == clerkUser.ID {
			userExists = true
			break
		}
	}
	if userExists {
		render.Render(w, r, dto.ErrInvalidRequest(errors.New("User already exists")))
		return
	}

	user := models.NewUser(clerkUser)
	playerCount := 0
	gmCount := 0
	for _, user := range Event.EventUsers {
		if user.Status == models.Player{
			playerCount++
		}
	}
	for _, user := range Event.EventUsers {
		if user.Status == models.GM{
			gmCount++
		}
	}
	if playerCount < int(*Event.MaxPlayers) {
		user.Status = models.Player
	} else if gmCount < int(*Event.MaxGms) {
		user.Status = models.GM
	} else {
		user.Status = models.Pending
	}

	if _, err := h.Repo.AddUserByInviteLink(Event.ID, token, *user); err != nil {
		h.Log.Println((err.Error()))
		render.Render(w, r, dto.ErrInternalServer)
		return
	}
	render.Status(r, http.StatusNoContent)
}


func GenerateRandomString() (string, error) {
	b := make([]byte, 32)
	if _, err := rand.Read(b); err != nil {
        return "", err
    }
    return hex.EncodeToString(b), nil
}