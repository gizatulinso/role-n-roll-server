package server

import (
	// "context"
	"database/sql"

	"github.com/go-chi/chi/v5"
	"github.com/go-chi/chi/v5/middleware"
	"github.com/go-chi/cors"
	"github.com/go-chi/render"
	_ "gitlab.com/gizatulinso/role-n-roll-server/src/generated-sql/postgres/public/model"
	// "gitlab.com/gizatulinso/role-n-roll-server/src/repositories"
	"gitlab.com/gizatulinso/role-n-roll-server/src/handlers"
	"gitlab.com/gizatulinso/role-n-roll-server/src/util"
	"log"
	"net/http"
)

type App struct {
	db   *util.DB
	Log  *log.Logger
	Port string
	sql  *sql.DB
}

func NewApp(db *util.DB, log *log.Logger, port string) *App {
	return &App{
		db:   db,
		Log:  log,
		Port: port,
		sql:  db.DB,
		// h: handlers.EventHandler{DB: db, Log: log},
	}
}

func (app *App) Start() {
	r := chi.NewRouter()

	r.Use(cors.Handler(cors.Options{
		AllowedOrigins:   []string{"*"},
		AllowedMethods:   []string{"GET", "POST", "PUT", "DELETE", "OPTIONS"},
		AllowedHeaders:   []string{"Accept", "Authorization", "Content-Type", "X-CSRF-Token"},
		ExposedHeaders:   []string{"Link"},
		AllowCredentials: true,
		MaxAge:           300,
	}))
	r.Use(middleware.RequestID)
	r.Use(middleware.RealIP)
	r.Use(middleware.Logger)
	r.Use(middleware.Recoverer)
	r.Use(render.SetContentType(render.ContentTypeJSON))
	r.Use(handlers.WithClerkSession)

	r.Get("/", func(w http.ResponseWriter, r *http.Request) {
		w.Write([]byte("Hello, world!"))
	})

	var h handlers.EventHandler = handlers.NewEventHandler(
		app.sql,
		app.Log,
	)

	r.Route("/api", func(r chi.Router) {
		r.Route("/events", func(r chi.Router) {
			r.Get("/", h.GetAllEvents)
			r.Post("/", h.CreateEvent)
			r.Route("/{ID}", func(r chi.Router) {
				r.Use(h.WithEventCtx)
				r.Get("/", h.GetEvent)
				r.Put("/", h.UpdateEvent)
				r.Delete("/", h.DeleteEvent)

				r.Route("/users", func(r chi.Router) {
					r.Post("/", h.AddUserToEvent)
					r.Route("/{UserID}", func(r chi.Router) {
						r.Put("/", h.UpdateUserStatus)
						r.Delete("/", h.RemoveUserFromEvent)
					})
				})

				r.Route("/comments", func(r chi.Router) {
					r.Get("/", h.GetComments)
					r.Post("/", h.AddCommentToEvent)
					r.Route("/{CommentID}", func(r chi.Router) {
						r.Use(h.WithCommentCtx)
						r.Put("/", h.UpdateComment)
						r.Delete("/", h.DeleteComment)
					})
				})

				r.Route("/invites", func(r chi.Router) {
					r.Post("/", h.CreateInviteLink)
					r.Route("/{Token}", func(r chi.Router) {
						r.Put("/", h.AddUserByInviteLink)
					})
				})
			})
		})
	})

	app.Log.Println("Starting server on port " + app.Port)
	http.ListenAndServe(":"+app.Port, r)
}
