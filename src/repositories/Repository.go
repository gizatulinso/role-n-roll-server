package repositories

type Repository[T any] interface {
	GetByID(ID int) (*T, error)
	GetAll(pageNumber int, itemsPerPage int) ([]*T, error)
	Save(event *T) (int32, error)
	Delete(ID int32) error
	Update(event *T) error
}
