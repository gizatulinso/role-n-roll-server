package repositories

import (
	// "context"
	"database/sql"
	// "fmt"
	. "github.com/go-jet/jet/v2/postgres"
	. "gitlab.com/gizatulinso/role-n-roll-server/src/generated-sql/postgres/public/table"
	"gitlab.com/gizatulinso/role-n-roll-server/src/models"
)

type EventRepository interface {
	Repository[models.Event]
	
	AddUser(eventID int32, user *models.User) (int32, error)
	DeleteUser(userID int32) error
	UpdateUserStatus(userID int32, status models.UserStatus) error
	GetComment(commentID int32) (*models.Comment, error)
	// GetComments returns a flat list of comments for an event
	GetComments(eventID int32) ([]*models.Comment, error)
	AddComment(eventID int32, comment *models.Comment) (int32, error)
	DeleteComment(commentID int32) error
	UpdateComment(comment *models.Comment) error
	AddInviteLink(eventID int32, token string) error
	AddUserByInviteLink(eventID int32, token string, user models.User) (int32, error)
}

type EventRepositoryImpl struct {
	DB *sql.DB
}

func NewEventRepository(db *sql.DB) *EventRepositoryImpl {
	return &EventRepositoryImpl{
		DB: db,
	}
}

func (r *EventRepositoryImpl) GetByID(ID int) (*models.Event, error) {
	commentTree := CTE("comment_tree")
	stmt := WITH_RECURSIVE(
		commentTree.AS(
			SELECT(
				Comments.AllColumns,
			).FROM(
				Comments,
			).WHERE(
				Comments.EventId.EQ(Int(int64(ID))).
					AND(Comments.ParentId.IS_NULL()),
			).
				UNION_ALL(
					SELECT(
						Comments.AllColumns,
					).FROM(
						commentTree.
							INNER_JOIN(
								Comments,
								Comments.ID.From(commentTree).
									EQ(Comments.ParentId),
							),
					),
				),
		),
	)(
		SELECT(
			Events.AllColumns,
			EventUsers.AllColumns,
			EventInviteLinks.AllColumns,
			commentTree.AllColumns(),
		).FROM(
			Events.
				LEFT_JOIN(
					EventUsers,
					Events.ID.EQ(EventUsers.EventId),
				).
				LEFT_JOIN(
					EventInviteLinks,
					Events.ID.EQ(EventInviteLinks.EventId),
				).
				LEFT_JOIN(
					commentTree,
					Events.ID.EQ(Comments.EventId.From(commentTree)),
				),
		).WHERE(
			Events.ID.EQ(Int(int64(ID))),
		),
	)

	event := &models.Event{}
	if err := stmt.Query(r.DB, event); err != nil {
		return nil, err
	}

	event.Comments = buildCommentThreadHierarchy(event.Comments)
	
	return event, nil
}


func (r *EventRepositoryImpl) GetAll(pageNumber int, itemsPerPage int) ([]*models.Event, error) {
	offset := (int64(pageNumber) - 1) * int64(itemsPerPage)

	stmt := SELECT(
		Events.AllColumns,
		EventUsers.AllColumns,
	).FROM(
		Events.LEFT_JOIN(
				EventUsers,Events.ID.EQ(EventUsers.EventId),
			),
	).LIMIT(int64(itemsPerPage)).OFFSET(offset)

	events := []*models.Event{}
	if err := stmt.Query(r.DB, &events); err != nil {
		return nil, err
	}

	return events, nil
}

func (r *EventRepositoryImpl) Save(event *models.Event) (int32, error) {
	stmt := Events.
		INSERT(Events.MutableColumns).
		MODEL(event).
		RETURNING(Events.ID)

	dest := &models.Event{}
	if err := stmt.Query(r.DB, dest); err != nil {
		return 0, err
	} else {
		return dest.ID, nil
	}
}

func (r *EventRepositoryImpl) Delete(ID int32) error {
	stmt := Events.DELETE().
		WHERE(Events.ID.EQ(Int(int64(ID))))

	if _, err := stmt.Exec(r.DB); err != nil {
		return err
	}

	return nil
}

func (r *EventRepositoryImpl) Update(event *models.Event) error {
	stmt := Events.
		UPDATE(Events.MutableColumns).
		MODEL(event).
		WHERE(Events.ID.EQ(Int(int64(event.ID))))

	if _, err := stmt.Exec(r.DB); err != nil {
		return err
	}

	return nil
}

func (r *EventRepositoryImpl) AddUser(eventID int32, user *models.User) (int32, error) {
	stmt := EventUsers.
		INSERT(EventUsers.MutableColumns).
		MODEL(user).
		RETURNING(EventUsers.ID)

	dest := &models.User{}
	if err := stmt.Query(r.DB, dest); err != nil {
		return 0, err
	} else {
		return dest.ID, nil
	}
}

func (r *EventRepositoryImpl) DeleteUser(userID int32) error {
	stmt := EventUsers.
		DELETE().
		WHERE(
			EventUsers.ID.EQ(Int(int64(userID))),
		)

	if _, err := stmt.Exec(r.DB); err != nil {
		return err
	}

	return nil
}

func (r *EventRepositoryImpl) UpdateUserStatus(userID int32, status models.UserStatus) error {
	stmt := EventUsers.
		UPDATE(EventUsers.Status).
		SET(status).
		WHERE(EventUsers.ID.EQ(Int(int64(userID))))

	if _, err := stmt.Exec(r.DB); err != nil {
		return err
	}

	return nil
}

// GetComments returns a flat list of comments for an event
func (r *EventRepositoryImpl) GetComments(eventID int32) ([]*models.Comment, error) {
	commentTree := CTE("comment_tree")
	stmt := WITH_RECURSIVE(
		commentTree.AS(
			SELECT(
				Comments.AllColumns,
			).FROM(
				Comments,
			).WHERE(
				Comments.EventId.EQ(Int(int64(eventID))).
					AND(Comments.ParentId.IS_NULL()),
			).
				UNION_ALL(
					SELECT(
						Comments.AllColumns,
					).FROM(
						commentTree.
							INNER_JOIN(
								Comments,
								Comments.ID.From(commentTree).
									EQ(Comments.ParentId),
							),
					),
				),
		),
	)(
		SELECT(
			commentTree.AllColumns(),
		).FROM(
			commentTree,
		),
	)

	comments := []*models.Comment{}
	if err := stmt.Query(r.DB, &comments); err != nil {
		return nil, err
	}

	return buildCommentThreadHierarchy(comments), nil
}

func (r *EventRepositoryImpl) AddComment(eventID int32, comment *models.Comment) (int32, error) {
	stmt := Comments.
		INSERT(Comments.MutableColumns).
		MODEL(comment).
		RETURNING(Comments.ID)

	dest := &models.Comment{}
	if err := stmt.Query(r.DB, dest); err != nil {
		return 0, err
	} else {
		return dest.ID, nil
	}
}

func (r *EventRepositoryImpl) DeleteComment(commentID int32) error {
	stmt := Comments.
		DELETE().
		WHERE(
			Comments.ID.EQ(Int(int64(commentID))),
		)

	if _, err := stmt.Exec(r.DB); err != nil {
		return err
	}

	return nil
}

func (r *EventRepositoryImpl) GetComment(commentID int32) (*models.Comment, error) {
	stmt := SELECT(
		Comments.AllColumns,
	).FROM(
		Comments,
	).WHERE(
		Comments.ID.EQ(Int(int64(commentID))),
	)

	comment := &models.Comment{}
	if err := stmt.Query(r.DB, comment); err != nil {
		return nil, err
	}

	return comment, nil
}

func (r *EventRepositoryImpl) UpdateComment(comment *models.Comment) error {
	stmt := Comments.
		UPDATE(Comments.AllColumns).
		MODEL(comment).
		WHERE(Comments.ID.EQ(Int(int64(comment.ID))))

	if _, err := stmt.Exec(r.DB); err != nil {
		return err
	}

	return nil
}

func (r *EventRepositoryImpl) AddInviteLink(eventID int32, token string) error {
	stmt := EventInviteLinks.
		INSERT(EventInviteLinks.EventId, EventInviteLinks.Token).
		VALUES(eventID, token)

	if _, err := stmt.Exec(r.DB); err != nil {
		return err
	}

	return nil
}

func (r *EventRepositoryImpl) AddUserByInviteLink(eventID int32, token string, user models.User) (int32, error) {
	user.EventId = eventID
	stmt := EventUsers.
		INSERT(EventUsers.MutableColumns).
		MODEL(user).
		RETURNING(EventUsers.ID)

	dest := &models.User{}
	if err := stmt.Query(r.DB, dest); err != nil {
		return 0, err
	} else {
		return dest.ID, nil
	}
}


func buildCommentThreadHierarchy(comments []*models.Comment) []*models.Comment {
	m := make(map[int]*models.Comment)
	for _, item := range comments {
		m[int(item.ID)] = item
	}

	for _, item := range comments {
		item.Children = []*models.Comment{}
		if item.ParentId != nil {
			parent, ok := m[int(*item.ParentId)]
			if ok {
				parent.Children = append(parent.Children, item)
			}
		}
	}
	

	var result []*models.Comment
	for _, item := range comments {
		if item.ParentId == nil {
			result = append(result, item)
		}
	}

	return result
}
