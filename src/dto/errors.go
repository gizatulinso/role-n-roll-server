package dto

import (
	"github.com/go-chi/render"
	"net/http"
)

type ErrResponse struct {
	Err            error `json:"-"`
	HTTPStatusCode int   `json:"-"`

	StatusText string `json:"status"`
	AppCode    int64  `json:"code,omitempty"`
	ErrorText  string `json:"error,omitempty"` // application-level error message, for debugging
}

func (e *ErrResponse) Render(w http.ResponseWriter, r *http.Request) error {
	render.Status(r, e.HTTPStatusCode)
	return nil
}

func ErrInvalidRequest(err error) render.Renderer {
	return &ErrResponse{
		Err:            err,
		HTTPStatusCode: 400,
		StatusText:     "Invalid request.",
		ErrorText:      err.Error(),
	}
}

var ErrNotFound = &ErrResponse{
	HTTPStatusCode: 404,
	StatusText:     "Not Found",
	ErrorText:      "Resource not found.",
}

var ErrInternalServer = &ErrResponse{
	HTTPStatusCode: 500,
	StatusText:     "Internal Server Error",
	ErrorText:      "Oops! Something went wrong.",
}

var ErrNotAuthorized = &ErrResponse{
	HTTPStatusCode: 401,
	StatusText:     "Not Authorized",
	ErrorText:      "You are not authorized to perform this action.",
}