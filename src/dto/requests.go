package dto

import (
	"net/http"
	"gitlab.com/gizatulinso/role-n-roll-server/src/models"
	"time"
	"fmt"
)

type POSTEventRequest struct {
	*models.Event
}

func (data *POSTEventRequest) Bind(r *http.Request) error {
	return nil
}

type PUTEventRequest struct {
	Description string `json:"description"`
	Title       string `json:"title"`
	ScheduledAt time.Time `json:"scheduledAt"`
	ScheduledAtTime string `json:"scheduledAtTime"`
	Language    string `json:"language"`
	IsPrivate   bool `json:"isPrivate"`
}

func (data *PUTEventRequest) Bind(r *http.Request) error {
	return nil
}

type POSTEventUserRequest struct {
	*models.User
}

func (data *POSTEventUserRequest) Bind(r *http.Request) error {
	return nil
}


type PUTUserStatusRequest struct {
	Status models.UserStatus `json:"status"`
}

func (data *PUTUserStatusRequest) Bind(r *http.Request) error {
	status := data.Status
	if !models.IsValidUserStatus(string(status)) {
		return fmt.Errorf("Invalid status")
	}
	return nil
}

type POSTCommentRequest struct {
	ID        int32      `sql:"primary_key" json:"id"`
	ClerkId   string     `json:"clerkId"`
	UserName  string     `json:"userName"`
	EventId   int32      `json:"eventId"`
	ParentId  *int32     `json:"parentId"`
	Content   string     `json:"content"`
}

func (data *POSTCommentRequest) Bind(r *http.Request) error {
	return nil
}

type PUTCommentRequest struct {
	Content string `json:"content"`
}

func (data *PUTCommentRequest) Bind(r *http.Request) error {
	return nil
}