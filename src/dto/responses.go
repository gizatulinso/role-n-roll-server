package dto

import (
	"github.com/go-chi/render"
	"gitlab.com/gizatulinso/role-n-roll-server/src/models"
	"net/http"
)

type EventResponse struct {
	*models.Event
}

func NewEventReponse(event *models.Event) *EventResponse {
	resp := &EventResponse{Event: event}
	return resp
}

func NewEventListResponse(events []*models.Event) []render.Renderer {
	list := []render.Renderer{}
	for _, event := range events {
		list = append(list, NewEventReponse(event))
	}
	return list
}

func (rd *EventResponse) Render(w http.ResponseWriter, r *http.Request) error {
	return nil
}

type CommentResponse struct {
	*models.Comment
}

func NewCommentReponse(comment *models.Comment) *CommentResponse {
	resp := &CommentResponse{Comment: comment}
	return resp
}

// NewCommentListResponse Transforms a flat list of comments into a hierarchical list of comments
func NewCommentListResponse(comments []*models.Comment) []render.Renderer {
	list := []render.Renderer{}
	for _, comment := range comments {
		list = append(list, NewCommentReponse(comment))
	}
	return list
}

func (rd *CommentResponse) Render(w http.ResponseWriter, r *http.Request) error {
	return nil
}
