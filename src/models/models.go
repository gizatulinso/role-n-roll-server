package models

import (
	generated "gitlab.com/gizatulinso/role-n-roll-server/src/generated-sql/postgres/public/model"
	"github.com/clerk/clerk-sdk-go/v2"
)

type Event struct {
	generated.Events
	EventUsers       []*User
	EventInviteLinks []*generated.EventInviteLinks
	Comments         []*Comment
}

type User struct {
	generated.EventUsers
	Status UserStatus
}

func NewUser(clerkUser *clerk.User) *User {
	user := &User{}
	user.ClerkId = clerkUser.ID
	user.FirstName = clerkUser.FirstName
	user.LastName = clerkUser.LastName
	user.ImageUrl = clerkUser.ImageURL
	var mail string 
	mailID := clerkUser.PrimaryEmailAddressID
	for _, email := range clerkUser.EmailAddresses {
		if email.ID == *mailID {
			mail = email.EmailAddress
		}
	}
	user.Email = &mail
	user.Status = Pending
	return user
}

type UserStatus string

const (
	Player  UserStatus = "player"
	GM      UserStatus = "gm"
	Pending UserStatus = "pending"
)

func IsValidUserStatus(status string) bool {
	switch status {
	case "player", "gm", "pending":
		return true
	default:
		return false
	}
}

type Comment struct {
	generated.Comments
	Children []*Comment
}


