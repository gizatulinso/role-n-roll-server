package util

import (
	"fmt"
	"github.com/go-jet/jet/v2/generator/metadata"
	"github.com/go-jet/jet/v2/generator/postgres"
	"github.com/go-jet/jet/v2/generator/template"
	postgres2 "github.com/go-jet/jet/v2/postgres"
	"github.com/iancoleman/strcase"
	_ "github.com/lib/pq"
	"strconv"
	"os"
    // "path/filepath"
)

func GenerateSQL() {
	var TargetDir = getPath()
	var dbConnection = getDbConnection()
	err := postgres.Generate(
		TargetDir,
		dbConnection,
		template.Default(postgres2.Dialect).
			UseSchema(func(schemaMetaData metadata.Schema) template.Schema {
				return template.DefaultSchema(schemaMetaData).
					UseModel(template.DefaultModel().
						UseTable(func(table metadata.Table) template.TableModel {
							return template.DefaultTableModel(table).
								UseField(func(columnMetaData metadata.Column) template.TableModelField {
									defaultTableModelField := template.DefaultTableModelField(columnMetaData)
									return defaultTableModelField.UseTags(
										fmt.Sprintf(`json:"%s"`, strcase.ToLowerCamel(columnMetaData.Name)),
									)
								})
						}).
						UseView(func(table metadata.Table) template.ViewModel {
							return template.DefaultViewModel(table).
								UseField(func(columnMetaData metadata.Column) template.TableModelField {
									defaultTableModelField := template.DefaultTableModelField(columnMetaData)
									if table.Name == "actor_info" && columnMetaData.Name == "actor_id" {
										return defaultTableModelField.UseTags(`sql:"primary_key"`)
									}
									return defaultTableModelField
								})
						}),
					)
			}),
	)

	if err != nil {
		panic(err)
	}
}

func getDbConnection () postgres.DBConnection {
	const (
		GeneratorHost = "GENERATOR_HOST"
		GeneratorPort = "GENERATOR_PORT"
		GeneratorUser = "GENERATOR_USER"
		GeneratorPassword = "GENERATOR_PASSWORD"
		GeneratorDbName = "GENERATOR_DB_NAME"
		GeneratorSSLMode = "GENERATOR_DB_SSLMODE"
		GeneratorSchema = "GENERATOR_DB_SCHEMA"
	)

	portStr := os.Getenv(GeneratorPort)
	port, _ := strconv.Atoi(portStr)

	return postgres.DBConnection{
		Host: os.Getenv(GeneratorHost),
		Port: port,
		User: os.Getenv(GeneratorUser),
		Password: os.Getenv(GeneratorPassword),
		DBName: os.Getenv(GeneratorDbName),
		SchemaName: os.Getenv(GeneratorSchema),
		// SslMode: os.Getenv(GeneratorSSLMode),
	}
}



func getPath() string {
    dir, err := os.Getwd()
    if err != nil {
        // panic(err)
    }
    return dir + "/src" + "/generated-sql"
}
