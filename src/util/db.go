package util

import (
	"database/sql"
	"io"
	"log"
	// "path/filepath"
	"time"

	_ "github.com/jackc/pgx/v5/stdlib"
)

type DBConfig struct {
	Url             string
	MaxOpenConns    int
	MaxIdleConns    int
	ConnMaxLifetime time.Duration
	ConnMaxIdleTime time.Duration
	Log             *log.Logger
}

type DB struct {
	DB *sql.DB
	DBConfig
}

func NewDB(cfg DBConfig) *DB {
	if cfg.Log == nil {
		cfg.Log = log.New(io.Discard, "", 0)
	}

	return &DB{
		DBConfig: cfg,
	}
}

func (db *DB) Open() error {
	
	DB, err := sql.Open("pgx", db.Url)
	if err != nil {
		db.Log.Fatal(err.Error())
		return err
	}
	db.Log.Println("Database connected: ,", db.Url)

	DB.SetMaxOpenConns(db.MaxOpenConns)
	DB.SetMaxIdleConns(db.MaxIdleConns)
	DB.SetConnMaxLifetime(db.ConnMaxLifetime * time.Minute)
	DB.SetConnMaxIdleTime(db.ConnMaxIdleTime * time.Minute)
	db.Log.Println("MaxOpenConns: ", db.MaxOpenConns)
	db.Log.Println("MaxIdleConns: ", db.MaxIdleConns)
	db.Log.Println("ConnMaxLifetime: ", db.ConnMaxLifetime)
	db.Log.Println("ConnMaxIdleTime: ", db.ConnMaxIdleTime)

	db.DB = DB
	
	// err = db.DB.Ping()
	// if err != nil {
	// 	db.Log.Fatal(err.Error())
	// 	return err
	// }

	return nil
}

