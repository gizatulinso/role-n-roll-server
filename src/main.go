package main

import (
	"errors"
	"log"
	"os"
	// "fmt"
	// "github.com/joho/godotenv"
	"gitlab.com/gizatulinso/role-n-roll-server/src/server"
	"gitlab.com/gizatulinso/role-n-roll-server/src/util"
	"strconv"
	"time"
)

func main() {
	// // err := godotenv.Load()
	// if err != nil {
	// 	panic(err)
	// }

	if len(os.Args) < 3 {
		dbConfig, err := getDbConfig()
		if err != nil {
			panic(err)
		}

		db := util.NewDB(*dbConfig)

		if err := db.Open(); err != nil {
			log.Fatal("Error opening database")
			panic(err)
		}
		PORT := os.Getenv("PORT")
		app := server.NewApp(db, log.New(os.Stdout, "APP: ", log.LstdFlags), PORT)

		app.Start()
	} else {
		util.GenerateSQL()
	}
}

func getDbConfig() (*util.DBConfig, error) {
	const (
		DBMaxOpenConnections = "DB_MAX_OPEN_CONNECTIONS"
		DBMaxIdleConnections = "DB_MAX_IDLE_CONNECTIONS"
		DBMaxIdleTime        = "DB_MAX_IDLE_TIME"
		DBURL                = "DB_URL"
		DBSSLMode            = "DB_SSL_MODE"
	)

	DB_URL := os.Getenv("DB_URL")

	println(DB_URL)
	if DB_URL == "" {
		err := errors.New("DB_URL is not set")
		log.Fatal(err.Error())
		return nil, err
	}

	DB_MAX_OPEN_CONNECTIONS, err := strconv.Atoi(os.Getenv(DBMaxOpenConnections))
	if err != nil {
		log.Fatal("DB_MAX_OPEN is not set.", err.Error())
		return nil, err
	}
	DB_MAX_IDLE_CONNECTIONS, err := strconv.Atoi(os.Getenv(DBMaxIdleConnections))
	if err != nil {
		log.Fatal("DB_MAX_IDLE_CONNECTIONS is not set.", err.Error())
		return nil, err
	}

	DB_MAX_IDLE_TIME, err := strconv.Atoi(os.Getenv(DBMaxIdleTime))
	if err != nil {
		log.Fatal("DB_MAX_IDLE_TIME is not set.", err.Error())
		return nil, err
	}
	DB_MAX_LIFETIME_CONNECTIONS, err := strconv.Atoi(os.Getenv("DB_MAX_LIFETIME_CONNECTIONS"))
	if err != nil {
		log.Fatal("DB_MAX_LIFETIME_CONNECTIONS is not set.", err.Error())
		return nil, err
	}

	DB_SSL_MODE := os.Getenv(DBSSLMode)
	if DB_SSL_MODE == "true"  || DB_SSL_MODE == "enabled" || DB_SSL_MODE == "enable" {
		DB_URL = DB_URL + "?sslmode=enable"
	}

	cfg := util.DBConfig{
		Url:             DB_URL,
		MaxIdleConns:    DB_MAX_OPEN_CONNECTIONS,
		MaxOpenConns:    DB_MAX_IDLE_CONNECTIONS,
		ConnMaxLifetime: time.Duration(DB_MAX_LIFETIME_CONNECTIONS) * time.Second,
		ConnMaxIdleTime: time.Duration(DB_MAX_IDLE_TIME) * time.Second,
		Log:             log.New(os.Stdout, "DB: ", log.LstdFlags),
	}
	return &cfg, nil
}
