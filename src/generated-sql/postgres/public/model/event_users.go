//
// Code generated by go-jet DO NOT EDIT.
//
// WARNING: Changes to this file may cause incorrect behavior
// and will be lost if the code is regenerated
//

package model

import (
	"time"
)

type EventUsers struct {
	ID        int32      `sql:"primary_key" json:"id"`
	EventId   int32      `json:"eventId"`
	ClerkId   string     `json:"clerkId"`
	UserName  string     `json:"userName"`
	FirstName *string    `json:"firstName"`
	LastName  *string    `json:"lastName"`
	Status    *string    `json:"status"`
	Email     *string    `json:"email"`
	ImageUrl  *string    `json:"imageUrl"`
	CreatedAt time.Time  `json:"createdAt"`
	UpdatedAt *time.Time `json:"updatedAt"`
}
