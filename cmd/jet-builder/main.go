package main

import (
	"fmt"
	"os"
	"strconv"
	// "strings"
	"github.com/go-jet/jet/v2/generator/metadata"
	"github.com/go-jet/jet/v2/generator/postgres"
	"github.com/go-jet/jet/v2/generator/template"
	// "github.com/go-jet/jet/v2/internal/jet"
	postgres2 "github.com/go-jet/jet/v2/postgres"
	"github.com/iancoleman/strcase"
	"github.com/joho/godotenv"
	_ "github.com/lib/pq"
	// "github.com/go-jet/jet/v2/internal/utils/strslice"
	// "path/filepath"
)

func main() {
	err := godotenv.Load()
	if err != nil {
		panic(err)
	} 

	var destDir = getPath()
	var dbConnection = getDbConnection()
	var template = getTemplate()

	err = postgres.Generate(
		destDir,
		dbConnection,
		template,
	)
	if err != nil {
		panic(err)
	}
}

func GenerateSQL() {
	var destDir = getPath()
	var dbConnection = getDbConnection()
	var template = getTemplate()

	err := postgres.Generate(
		destDir,
		dbConnection,
		template,
	)
	if err != nil {
		panic(err)
	}
}


func getDbConnection() postgres.DBConnection {
	const (
		GeneratorHost     = "GENERATOR_HOST"
		GeneratorPort     = "GENERATOR_PORT"
		GeneratorUser     = "GENERATOR_USER"
		GeneratorPassword = "GENERATOR_PASSWORD"
		GeneratorDbName   = "GENERATOR_DB_NAME"
		GeneratorSSLMode  = "GENERATOR_DB_SSLMODE"
		GeneratorSchema   = "GENERATOR_DB_SCHEMA"
	)

	portStr := os.Getenv(GeneratorPort)
	port, _ := strconv.Atoi(portStr)

	return postgres.DBConnection{
		Host:       os.Getenv(GeneratorHost),
		Port:       port,
		User:       os.Getenv(GeneratorUser),
		Password:   os.Getenv(GeneratorPassword),
		DBName:     os.Getenv(GeneratorDbName),
		SchemaName: os.Getenv(GeneratorSchema),
		// SslMode: os.Getenv(GeneratorSSLMode),
	}
}

func getPath() string {
	dir, err := os.Getwd()
	if err != nil {
		// panic(err)
	}
	return dir + "/generated" + "/"
}

func getTemplate() template.Template {
	shouldSkipTable := func(table metadata.Table) bool {
		return table.Name == "_prisma_migrations"
	}

	template := template.Default(postgres2.Dialect).
		UseSchema(func(schemaMetaData metadata.Schema) template.Schema {
			return template.DefaultSchema(schemaMetaData).
				UseModel(template.DefaultModel().
					UseTable(func(table metadata.Table) template.TableModel {
						if shouldSkipTable(table) {
							return template.TableModel{Skip: true}
						}
						return template.DefaultTableModel(table).
							UseField(func(columnMetaData metadata.Column) template.TableModelField {
								defaultTableModelField := template.DefaultTableModelField(columnMetaData)
								return defaultTableModelField.UseTags(
									fmt.Sprintf(`json:"%s"`, strcase.ToLowerCamel(columnMetaData.Name)),
								)
							})
					}),
				).
				UseSQLBuilder(
					template.DefaultSQLBuilder().
						UseTable(func(table metadata.Table) template.TableSQLBuilder {
							if shouldSkipTable(table) {
								return template.TableSQLBuilder{Skip: true}
							}
							return template.DefaultTableSQLBuilder(table)
						}),
				)
		})

	return template
}