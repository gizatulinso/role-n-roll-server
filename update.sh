#!/bin/bash

# Decode the SSH key
DECODED_SSH_KEY=$(echo "$ENCODED_SSH_KEY" | base64 -d)

# Create the directory for the key if it doesn't exist
mkdir -p ./key

# Write the decoded key to a file
echo "$DECODED_SSH_KEY" > ./key/key.pem

# Set the permissions for the key and its directory
chmod 600 /ansible/key/key.pem
chmod 700 /ansible/key

# Run the Docker container with the key mounted to the /ansible/key directory
docker run -it \
    -v ./key:/ansible/key \
    ubuntu:latest \
    /ansible/env/bin/ansible-playbook -i /ansible/inventory /ansible/playbook.yml -e ssh_key="/ansible/key/key.pem"